<?php
namespace FBoon\ViesCheckBundle;

use OnlineQ\Finance\ViesCheckBundle\Model\VatResponse;
use OnlineQ\Finance\ViesCheckBundle\Model\VatApproxResponse;
/**
 * Simple client to check vat-numbers
 *
 * @author Frank Boon <boon.frank@gmail.com>
 */
class Client
{
    /**
     * @var \SoapClient
     */
    protected $soapClient;

    public function __construct($uri = "http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl")
    {
        $this->soapClient = new \SoapClient($uri);
    }

    /**
     *
     * @param string $countryCode
     * @param string $vatNumber
     * @return \OnlineQ\Finance\ViesCheckBundle\Model\VatResponse
     */
    public function checkVat($countryCode, $vatNumber)
    {
        $params = array(
            'countryCode' => $countryCode,
            'vatNumber' => $vatNumber
        );

        $result = $this->soapClient->checkVat($params);

        $response = new VatResponse();
        $response->setAddress($result->address);
        $response->setName($result->name);
        $response->setValid($result->valid);
        $response->setRequestDate($result->requestDate);
        $response->setVatNumber($result->vatNumber);
        $response->setCountryCode($result->countryCode);

        return $response;
    }

    /**
     *
     * @param string $countryCode
     * @param string $vatNumber
     * @return \OnlineQ\Finance\ViesCheckBundle\Model\VatApproxResponse
     */
    public function checkVatApprox($countryCode, $vatNumber)
    {
        $params = array(
            'countryCode' => $countryCode,
            'vatNumber' => $vatNumber
        );

        $result = $this->soapClient->checkVatApprox($params);

        $response = new VatApproxResponse();
        $response->setValid($result->valid);
        $response->setRequestDate($result->requestDate);
        $response->setVatNumber($result->vatNumber);
        $response->setCountryCode($result->countryCode);
        $response->setTraderAddress($result->traderAddress);
        $response->setTraderCompanyType($result->traderCompanyType);
        $response->setTraderName($result->traderName);

        return $response;
    }
}
