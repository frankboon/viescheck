<?php
namespace FBoon\ViesCheckBundle\Model;

/**
 * Description of VatResponse
 *
 * @author Frank Boon <boon.frank@gmail.com>
 */
class VatApproxResponse
{
    protected $countryCode;

    protected $vatNumber;

    protected $requestDate;

    protected $valid;

    protected $traderName;

    protected $traderCompanyType;

    protected $traderAddress;

    protected $requestIdentifier;

    public function getCountryCode()
    {
        return $this->countryCode;
    }

    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    public function setVatNumber($vatNumber)
    {
        $this->vatNumber = $vatNumber;
    }

    public function getRequestDate()
    {
        return $this->requestDate;
    }

    public function setRequestDate($requestDate)
    {
        $this->requestDate = $requestDate;
    }

    public function getValid()
    {
        return $this->valid;
    }

    public function setValid($valid)
    {
        $this->valid = $valid;
    }

    public function getTraderName()
    {
        return $this->traderName;
    }

    public function setTraderName($traderName)
    {
        $this->traderName = $traderName;
    }

    public function getTraderCompanyType()
    {
        return $this->traderCompanyType;
    }

    public function setTraderCompanyType($traderCompanyType)
    {
        $this->traderCompanyType = $traderCompanyType;
    }

    public function getTraderAddress()
    {
        return $this->traderAddress;
    }

    public function setTraderAddress($traderAddress)
    {
        $this->traderAddress = $traderAddress;
    }

    public function getRequestIdentifier()
    {
        return $this->requestIdentifier;
    }

    public function setRequestIdentifier($requestIdentifier)
    {
        $this->requestIdentifier = $requestIdentifier;
    }
}
